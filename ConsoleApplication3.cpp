#include <iostream>

//void PrintNumbers(int a, int n)
//{
//    switch (a)
//    {
//    case 0:
//        for (int i = 0; i <= n; ++i)
//        {
//            if (i % 2 == 0)
//            {
//                std::cout << i << std::endl;
//            }
//        }
//        break;
//    case 1:
//        for (int i = 0; i <= n; ++i)
//        {
//            if (i % 2 != 0)
//            {
//                std::cout << i << std::endl;
//            }
//        }
//        break;
//    default:
//        std::cout << "Sorry, you entered something wrong. Let's try again" << std::endl;
//        break;
//    }
//}

void PrintNumbers(int a, int n)
{
    for (int i = 0; i <= n; )
    {
        if (a == 2) 
        {
            std::cout << i++ << std::endl;
            ++i;
        }
        else if (a == 1 && i <= n - 1) // ������� ��� ����, ����� �������� ����� �� 0 �� N (n) ������������
        {
            std::cout << ++i << std::endl;
            ++i;
        }
    }
}

int main()
{
    const int N = 10;
    int EvenOdd;
    int Numbers;

    std::cout << "You can see the sequence of even numbers from 0 to 10" << std::endl << std::endl;

    for (int i = 0; i <= N; ++i)
    {
        if (i % 2 == 0)
        {
            std::cout << i << std::endl;
        }
    }

    std::cout << std::endl << "You can make the sequence of even or odd numbers from 0 to 32 767" << std::endl;
    std::cout << "If you want even numbers enter 2. If you want odd numbers enter 1" << std::endl;
    std::cin >> EvenOdd;
    std::cout << "Please, enter upper limit number from 0 to 32 767" << std::endl;
    std::cin >> Numbers;

    if (Numbers < 0 || (2 < EvenOdd || EvenOdd < 1))
    {
        std::cout << std::endl << "Sorry, you entered something wrong. Let's try again" << std::endl;
    }
    else
    {
        std::cout << std::endl;
    }

    PrintNumbers(EvenOdd, Numbers);
}